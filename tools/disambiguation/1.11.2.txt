﻿* Minecraft
- armor_stand
- bone
- brown_mushroom;Brown
- clay;Block
#- clay_ball;Item
- cooked_mutton
- cooked_rabbit
- double_plant # Sunflower
- double_plant:1 # Lilac
- double_plant:5 # Peony
- end_bricks # End Stone Bricks
+ end_portal;End Portal
- ender_chest
- fish:1 # Raw Salmon
- fish:2 # Clownfish
- fish:3 # Pufferfish
+ golden_apple:1;Enchanted %name% # Enchanted Golden Apple
- iron_nugget # Iron Nugget
- lead
- log2 # Acacia Wood
- melon;Item
- melon_block;Block
- milk_bucket
- mutton # Raw Mutton
- nether_brick;Block
- netherbrick;Item
- planks:4 # Acacia Wood Planks
+ potion:8225;%name% II # Potion of Regeneration II
+ potion:8226;%name% II # Potion of Swiftness II
+ potion:8228;%name% II # Potion of Poison II
+ potion:8229;%name% II # Potion of Healing II
+ potion:8233;%name% II # Potion of Strength II
+ potion:8236;%name% II # Potion of Harming II
+ potion:16417;%name% II # Splash Potion of Regeneration II
+ potion:16418;%name% II # Splash Potion of Swiftness II
+ potion:16420;%name% II # Splash Potion of Poison II
+ potion:16421;%name% II # Splash Potion of Healing II
+ potion:16425;%name% II # Splash Potion of Strength II
+ potion:16428;%name% II # Splash Potion of Harming II
- rabbit # Raw Rabbit
- record_11;;%name% (%info%)
- record_13;;%name% (%info%)
- record_blocks;;%name% (%info%)
- record_cat;;%name% (%info%)
- record_chirp;;%name% (%info%)
- record_far;;%name% (%info%)
- record_mall;;%name% (%info%)
- record_mellohi;;%name% (%info%)
- record_stal;;%name% (%info%)
- record_strad;;%name% (%info%)
- record_wait;;%name% (%info%)
- record_ward;;%name% (%info%)
- red_flower # Poppy
- red_flower:2 # Allium
- red_mushroom;Red
- sapling:4 # Acacia Sapling
- sponge
- stone_button;Stone
- stone_pressure_plate;Stone
- yellow_flower
- water_bucket
- wooden_button;Wood
- wooden_pressure_plate;Wood

* Quark
- arrow_ender # Ender Arrow
- basalt # Basalt
- basalt:1 # Polished Basalt
- basalt_wall # Basalt Wall
- biome_cobblestone # Brimstone
- brick_wall # Brick Wall
- charcoal_block # Block of Charcoal
- glowshroom # Glowshroom
- iron_ladder # Iron Ladder
- iron_plate # Iron Plate
- iron_rod # Iron Rod
- limestone # Limestone
- marble # Marble
- paper_wall # Paper Wall
- prismarine_bricks_slab # Prismarine Brick Slab
- prismarine_bricks_stairs # Prismarine Brick Stairs
- prismarine_dark_slab # Dark Prismarine Slab
- prismarine_dark_stairs # Dark Prismarine Stairs
- prismarine_rough_wall # Prismarine Wall
- prismarine_slab # Prismarine Slab
- prismarine_stairs # Prismarine Stairs
- quartz_wall # Quartz Wall
- rain_detector # Weather Sensor
- red_sandstone_smooth_slab # Polished Red Sandstone Slab
- rune # White Rune
- rune:1 # Orange Rune
- rune:4 # Yellow Rune
- rune:10 # Purple Rune
- rune:11 # Blue Rune
- rune:13 # Green Rune
- rune:14 # Red Rune
- rune:15 # Black Rune
- sandstone_new:1 # Sandstone Bricks
- sandstone_new:3 # Red Sandstone Bricks
- sandstone_wall # Sandstone Wall
- sandy_bricks_slab # Sandy Brick Slab
- sandy_bricks_stairs # Sandy Brick Stairs
- sandy_bricks_wall # Sandy Brick Wall
- snow_bricks # Snow Bricks
- stone_andesite_bricks_slab # Andesite Brick Slab
- stone_andesite_bricks_stairs # Andesite Brick Stairs
- stone_andesite_slab # Andesite Slab
- stone_andesite_stairs # Andesite Stairs
- stone_andesite_wall # Andesite Wall
- stone_basalt_bricks_slab # Basalt Brick Slab
- stone_basalt_bricks_stairs # Basalt Brick Stairs
- stone_basalt_slab # Basalt Slab
- stone_basalt_stairs # Basalt Stairs
- stone_diorite_bricks_slab # Diorite Brick Slab
- stone_diorite_bricks_stairs # Diorite Brick Stairs
- stone_diorite_slab # Diorite Slab
- stone_diorite_stairs # Diorite Stairs
- stone_diorite_wall # Diorite Wall
- stone_granite_bricks_slab # Granite Brick Slab
- stone_granite_bricks_stairs # Granite Brick Stairs
- stone_granite_slab # Granite Slab
- stone_granite_stairs # Granite Stairs
- stone_limestone_bricks_slab # Limestone Brick Slab
- stone_limestone_bricks_stairs # Limestone Brick Stairs
- stone_limestone_slab # Limestone Slab
- stone_limestone_stairs # Limestone Stairs
- stone_marble_slab # Marble Slab
- thatch # Thatch
- thatch_slab # Thatch Slab
- thatch_stairs # Thatch Stairs
- world_stone_bricks # Granite Bricks
- world_stone_bricks:1 # Diorite Bricks
- world_stone_bricks:2 # Andesite Bricks
- world_stone_bricks:3 # Basalt Bricks
- world_stone_bricks:4 # Marble Bricks
- world_stone_bricks:5 # Limestone Bricks

* Thermal Foundation
- material:833 # Tar
- material:16 # Diamond Nugget
- material:24 # Iron Gear
- material:25 # Gold Gear
- material:32 # Iron Plate
- material:33 # Gold Plate
- material:128 # Copper Ingot
- material:129 # Tin Ingot
- material:130 # Silver Ingot
- material:131 # Lead Ingot
- material:160 # Steel Ingot
- material:161 # Electrum Ingot
- material:162 # Invar Ingot
- material:163 # Bronze Ingot
- material:320 # Copper Plate
- material:321 # Tin Plate
- material:322 # Silver Plate
- material:323 # Lead Plate
- material:325 # Nickel Plate
- material:326 # Platinum Plate
- material:327 # Iridium Plate
- material:352 # Steel Plate
- material:353 # Electrum Plate
- material:354 # Invar Plate
- material:355 # Bronze Plate
- ore # Copper Ore
- ore:1 # Tin Ore
- ore:2 # Silver Ore
- ore:3 # Lead Ore
- ore_fluid # Oil Sand
- storage # Copper Block
- storage:1 # Tin Block
- storage:2 # Silver Block
- storage:3 # Lead Block

* Witchery

